// vue.config.js
module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
      ? '/ssenat-2020.volebnikalkulacka.cz/'
      : '/'
}